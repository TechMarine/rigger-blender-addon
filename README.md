# Rigger - blender addon

Blender addon - automatic rigging of 3D models, easily extensible.

## Installation

 1. Download Rigger
 2. Create Rigger.zip file from downloaded files
 3. Start Blender
 4. Go to Edit > Preferences > Add-ons > Install
 5. Pick Rigger.zip
 6. Enable Rigger
 7. In Blender press N and pick Rigger panel

## Contribution

If you want to contribute to this add-on, you can create new rigging methods. To create a new method, clone Rigger repository or unzip Rigger.zip
You will find new_method_template.py file. This file serves as method template. You need to rename the file and the Class inside. 
Then implement rigging method in rig() method. If you need to create some ui elements like parameter inputs or buttons use method additional_ui() and PropertyGroup below.
To automatically load the new method, move the new_method_file.py to **methods** folder. It must be placed in python scripts/Rigger/methods folder or you can create zip and install it.
