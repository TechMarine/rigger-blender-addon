# Rigger add-on for Blender
# Copyright (C) 2020  Rostislav Horák
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy
import math
from . rigger_method import rigger_method

class Rigger_Operator(bpy.types.Operator):
    bl_idname = "view3d.rigger_operator"
    bl_label = "Rigger operator"
    bl_description = "Automaticly Rigs selected model"

    def execute(self, context):
        ''' execute selected rigging method '''

        # get selected object to rig
        active_obj = context.active_object

        # rig
        rigger_method.implemented_methods[context.scene.rigger.methods].rig(active_obj)

        return{'FINISHED'}