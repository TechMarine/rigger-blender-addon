# Rigger add-on for Blender
# Copyright (C) 2020  Rostislav Horák
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy
from . rigger_method import rigger_method


class Rigger_Props(bpy.types.PropertyGroup):

    methods = bpy.props.EnumProperty(
        items = rigger_method.panel_list,
        name = 'methods'
    )



class Rigger_Panel(bpy.types.Panel):
    bl_idname = "Rigger_PT_panel"
    bl_label = "Rigger"
    bl_category = "Rigger"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'



    def draw(self, context):
        
        layout = self.layout
        layout.label(text="Rigging method")
        layout.prop(context.scene.rigger, "methods")
        layout.operator('view3d.rigger_operator', text="Start Rigger")

        layout.separator()
        # draw layout based on selected method method_additional_ui(layout)
        rigger_method.implemented_methods[context.scene.rigger.methods].additional_ui(layout, context)
