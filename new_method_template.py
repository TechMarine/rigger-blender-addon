# Rigger add-on for Blender
# Copyright (C) 2020  Rostislav Horák
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


'''
Method template for Blender rigger addon
file must be in folder Rigger/methods/ with name format method_*.py (replace * with method name) 
'''

import bpy
import rigger_method

## TODO rename ( MUST be same as file without .py)
class method_new(rigger_method):


    def rig(self, obj):
        ''' implement method to rig obj (obj -- active object) '''

        ## TODO implement rigging HERE


        
    # ui elements added to panel
    def additional_ui(self, layout, context):
    	''' add additional method UI elements'''
	    # use layout.box() for more elements

        # TODO OPTIONAL add ui elements to layout


# TODO OPTIONAL use to add properties to panel
'''
# TODO change <method_name> for method name
class <method_name>_Props(bpy.types.PropertyGroup):

    # TODO ADD PROPS
    <prop_name>: bpy.props.<prop_type>(<prop_parameters>)

# register and create pointer for reference
bpy.utils.register_class(<method_name>_Props)
bpy.types.Scene.<method_name> = bpy.props.PointerProperty(type=<method_name>_Props)
'''