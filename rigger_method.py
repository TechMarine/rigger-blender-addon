# Rigger add-on for Blender
# Copyright (C) 2020  Rostislav Horák
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


'''
parent class of rigging methods for Blender Rigger Addon
'''

import bpy
import os
import sys
import importlib

class rigger_method:
    # list of implemented methods, usable by addon
    implemented_methods = {} # dict{"str_name" : method}  has method object references (create object for each method on import)
    # holds information about rigging methods for UI panel dropdown menu
    panel_list = []

    # holds information on rigged object and its generated armature --> used for skinning or other processes after armature is created
    rigged_object = None
    armature = None


    def rig(self, obj):
        ''' implement method to create rig for obj (obj -- active object) '''

    def additional_ui(self, layout, context):
        ''' add additional method UI elements'''

    @staticmethod
    def import_methods(): # TODO call this on start (registration of addon???)
        ''' loads methods into implemented_methods from ./methods/ folder'''


        for f in os.scandir(os.path.dirname(__file__)+'/methods'):
            print("------module file: " + f.name)
            if f.name.startswith("method_") and f.name.endswith(".py"): 
                module_name = f.name.split('.')[0]

                # import method
                klass = getattr(importlib.import_module('.'+module_name, 'Rigger.methods'), module_name)
                print("add module: " + module_name)

                # add method instance to dict
                rigger_method.implemented_methods[module_name] = klass()
                print(rigger_method.implemented_methods)

        # create list of methods for dropdown
        for method in rigger_method.implemented_methods.keys():
            rigger_method.panel_list.append((method, method, ""))
