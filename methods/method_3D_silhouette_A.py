# Rigger add-on for Blender
# Copyright (C) 2020  Rostislav Horák
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


'''
Rigging method using modified 3D silhouette with A* for Blender rigger add-on
'''

import bpy
import math
from .. rigger_method import rigger_method
import sys
import site
import os
import statistics

if not os.path.exists(site.USER_SITE):
    os.makedirs(site.USER_SITE)
sys.path.insert(0, site.USER_SITE)

try:
    import triangle
except ImportError:
    try:
        import pip
        pip.main(['install', '--user', 'triangle'])
    except:
        from pip._internal import main
        main(['install', '--user', 'triangle'])
    finally:
        import triangle


import time


class method_3D_silhouette_A(rigger_method):


    # find primary(first) plaine, acording to higher variance
    def getPrimaryPlaine(self, mesh):
        
        # get lists of coordinates of vertices
        vertX = []
        for vert in mesh.vertices:
            vertX.append(vert.co.x)

        vertY = []
        for vert in mesh.vertices:
            vertY.append(vert.co.y)

        vertZ = []
        for vert in mesh.vertices:
            vertZ.append(vert.co.z)

        # calculate variances for x, y, z
        varX = statistics.variance(vertX)
        varY = statistics.variance(vertY)
        varZ = statistics.variance(vertZ)

        plaineXvar = varY+varZ 
        plaineYvar = varX+varZ 
        plaineZvar = varX+varY 

        print(f"x variance: {varX}")
        print(f"y variance: {varY}")
        print(f"z variance: {varZ}")

        plaine = 'x'
        
        maxVar = max([plaineXvar, plaineYvar, plaineZvar])

        if (plaineXvar == maxVar):
            plaine = 'x'
        elif (plaineYvar == maxVar):
            plaine = 'y'
        elif (plaineZvar == maxVar):
            plaine = 'z'

        # return paline with highest variance
        return plaine

    # define 3D silouette of model in primary plaine
    def get3Dsilhouette(self, mesh, primaryPlaine):
        # execute algorithm of creating 3D silhouette from mesh

        print("rigging object: " + mesh.name)

        # find highest vertex -- starting point
        if (primaryPlaine == 'x'):
            vertZ = []
            for vert in mesh.vertices:
                vertZ.append(vert.co.z)

            startVert = mesh.vertices[vertZ.index(max(vertZ))]
        elif (primaryPlaine == 'y'):
            vertZ = []
            for vert in mesh.vertices:
                vertZ.append(vert.co.z)

            startVert = mesh.vertices[vertZ.index(max(vertZ))]
        elif (primaryPlaine == 'z'):
            vertY = []
            for vert in mesh.vertices:
                vertY.append(vert.co.y)

            startVert = mesh.vertices[vertY.index(max(vertY))]


        currVert = startVert

        # starting edge(angle) is straight UP
        currVector = (0,1) # for plaine x => (y,z)

        usedVertices = []

        silhouette = {}
        silhouette["segments"] = []
        silhouette["vertices"] = [startVert]
        while True:

            # get all edges containing current vertex
            neighEdges = []
            for edge in mesh.edges:
                if (currVert.index in edge.vertices):
                    neighEdges.append(edge)

            print(f"neighEdges count: {len(neighEdges)}")

            # find longest edge r(distance)
            r = 0
            if primaryPlaine == 'x':
                for edge in neighEdges:
                    dist = math.sqrt((mesh.vertices[edge.vertices[0]].co.y - mesh.vertices[edge.vertices[1]].co.y)**2 
                                    + (mesh.vertices[edge.vertices[0]].co.z - mesh.vertices[edge.vertices[1]].co.z)**2)
                    if dist > r:
                        r = dist
            elif primaryPlaine == 'y':
                for edge in neighEdges:
                    dist = math.sqrt((mesh.vertices[edge.vertices[0]].co.x - mesh.vertices[edge.vertices[1]].co.x)**2 
                                    + (mesh.vertices[edge.vertices[0]].co.z - mesh.vertices[edge.vertices[1]].co.z)**2)
                    if dist > r:
                        r = dist
            elif primaryPlaine == 'z':
                for edge in neighEdges:
                    dist = math.sqrt((mesh.vertices[edge.vertices[0]].co.x - mesh.vertices[edge.vertices[1]].co.x)**2 
                                    + (mesh.vertices[edge.vertices[0]].co.y - mesh.vertices[edge.vertices[1]].co.y)**2)
                    if dist > r:
                        r = dist

            r *= bpy.context.scene.method_3D_silhouette.r_multiplier

            print(f"----  r: {r}")
            
            # find all vertices in r neighbourhood

            # make list of used vertices use below --> if vert not in usedVertices:
            usedVertices.append(currVert)

            neighVerts = []    
            for vert in mesh.vertices:
                if vert not in usedVertices:
                    if primaryPlaine == 'x':
                        dist = math.sqrt((vert.co.y - currVert.co.y)**2 + (vert.co.z - currVert.co.z)**2)
                    elif primaryPlaine == 'y':
                        dist = math.sqrt((vert.co.x - currVert.co.x)**2 + (vert.co.z - currVert.co.z)**2)
                    elif primaryPlaine == 'z':
                        dist = math.sqrt((vert.co.x - currVert.co.x)**2 + (vert.co.y - currVert.co.y)**2)

                    if dist <= r:
                        if dist > bpy.context.scene.method_3D_silhouette.r_ignore*r:
                            neighVerts.append(vert)
                        else:
                            usedVertices.append(vert)
            
            if len(neighVerts) == 0:           
                break

            print(f"neighVerts count: {len(neighVerts)}")   

            # go through neighbours and find the best angle --> find new vertex
            # go through vertices, find largest angle between starting edge and edge made up of starting vertex and neighboring vertex --> neighEdges

            maxAngle = 0
            maxVert = currVert
            maxVector = currVector
            for vert in neighVerts:
                # get second vector
                if primaryPlaine == 'x':
                    newVector = (vert.co.y - currVert.co.y, vert.co.z - currVert.co.z)
                elif primaryPlaine == 'y':
                    newVector = (vert.co.x - currVert.co.x, vert.co.z - currVert.co.z)
                elif primaryPlaine == 'z':
                    newVector = (vert.co.x - currVert.co.x, vert.co.y - currVert.co.y)

                # compute angle of vectors
                angle = math.atan2(currVector[1], currVector[0]) - math.atan2(newVector[1], newVector[0])
                if (angle < 0):
                    angle += 2*math.pi
                print(f"angle: {angle}")
                

                if (angle > maxAngle):
                    maxAngle = angle
                    maxVert = vert
                    maxVector = newVector

            currVector = (-maxVector[0],-maxVector[1])
            currVert = maxVert
            silhouette["vertices"].append(currVert)
            currVert.select = True
            
            print(f"silhouette vertices: {len(silhouette['vertices'])}")

            print(f"currVert: {currVert}")
            print(f"startVert: {startVert}")

            # repeat until starting point found
            if currVert == startVert:
                print("-----END silhouette-----")
                break


        # ensure connectivity of vertexes in mesh (if two succeeding vertexes dont share same edge, find shortest route)
        # go through silhouette vertexes and check if two succeeding vertexes share edge, if not find shortest path
        currVert = startVert

        for globalVert in silhouette["vertices"][1:]: 
                
                # for the first time currVert is start

                # 1: get end vertex -> globalVert

                # 2: get neighbours of currVert

                # get all edges containing current vertex
                neighEdges = []
                for edge in mesh.edges:
                    if (currVert.index in edge.vertices):
                        neighEdges.append(edge)

                print(f"neighEdges count: {len(neighEdges)}")
    

                # optimization --> try searching only smaller area surrounding r

                # determine all vertices in r neighbourhood
                neighVerts = [] 
                for edge in neighEdges:
                    for edgeVert in edge.vertices:
                        if edgeVert != currVert.index:
                            neighVerts.append(mesh.vertices[edgeVert])   

                print(f"silhouette vertices: {len(silhouette['vertices'])}")
                print(f"silhouette edges: {len(silhouette['segments'])}")

                print(f"neighVerts count: {len(neighVerts)}")

                print(f"globalVert: {globalVert}")
                print("neighVerts:")
                for vert in  neighVerts:
                    print(f"-- {vert}")

                # 3: if endVert is neighbour continue to 4
                #  else get max angle neighbour and set as new currVert and save it to silhouette, jump to 2
                if globalVert in neighVerts:
                    silhouette["segments"].append([currVert.index, globalVert.index])
                    currVert = globalVert
                    print("------ is neighbour!")
                    
                    
                else:    
                    print("------ NOT neighbour! --> A* START")
                    # A* to find path between two silhouette vertices
                    open = [] # list of nodes: [score, distFromStart, vertex, [path to(list of previous vertices)]]
                    closed = [] # list of processed nodes
                    
                    # add neighbours of starting vertex to open
                    for vert in neighVerts:
                        score = math.sqrt((globalVert.co.y - vert.co.y)**2 + (globalVert.co.z - vert.co.z)**2) # distance to end vertex
                        distFromStart = math.sqrt((currVert.co.y - vert.co.y)**2 + (currVert.co.z - vert.co.z)**2) # distance from start
                        score += distFromStart
                        open.append([score, distFromStart, vert, []])


                    while open:
                        print(f'open: {len(open)}')

                        # get vertex with smallest score
                        open.sort(key=lambda x:(x[0])) # sort acording to score
                        currNode = open[0]
                        open.pop(0)
                        closed.append(currNode[2])

                        print(f'currNode: {currNode[2]}')

                        # find neighbours of node
                        neighEdges = []
                        for edge in mesh.edges:
                            if (currNode[2].index in edge.vertices):
                                neighEdges.append(edge)

                        neighVerts = [] 
                        for edge in neighEdges:
                            for edgeVert in edge.vertices:
                                if edgeVert != currNode[2].index:
                                    neighVerts.append(mesh.vertices[edgeVert])

                        # add new neighbours to open
                        for vert in neighVerts:
                            if vert not in closed:

                                score = math.sqrt((globalVert.co.y - vert.co.y)**2 + (globalVert.co.z - vert.co.z)**2) # distance to end vertex
                                distFromStart = math.sqrt((currNode[2].co.y - vert.co.y)**2 + (currNode[2].co.z - vert.co.z)**2) + currNode[1] # distance from start
                                score += distFromStart
                                
                                newPath = []
                                for pathElement in currNode[3]:
                                    newPath.append(pathElement)
                                newPath.append(currNode[2])

                                # look if vertex is already in open
                                inOpen = False
                                for node in open:
                                    if node[2] == vert:
                                        # update score and path if new score is lower and break
                                        if node[0] < score:
                                            node[0] = score
                                            node[1] = distFromStart
                                            node[3] = newPath
                                        
                                        inOpen = True
                                        break

                                if not inOpen:
                                    open.append([score, distFromStart, vert, newPath])

                        # if end vertex found, break
                        for node in open:
                            if node[2] == globalVert:
                                for vert in node[3]:
                                    if vert not in silhouette:
                                        print(f'new vert: {vert}')
                                        silhouette["vertices"].append(vert)
                                        silhouette["segments"].append([currVert.index, vert.index])
                                        currVert = vert

                                silhouette["segments"].append([currVert.index, globalVert.index])
                                # set globalVert as current
                                currVert = globalVert
                                open.clear() # end A*
                                print("----END A* ---")




        # add last edge --> close silhouette
        silhouette["segments"].append([currVert.index, startVert.index])
       
        print("-----COMPLETE silhouette-----")
 
        print(f"silhouette vertices: {len(silhouette['vertices'])}")
        print(f"silhouette edges: {len(silhouette['segments'])}")


        # returns silhouette -- lists of vertices, edges(segments)
        return silhouette


    # deulaneys triangulation on 3D silouette
    def deulaneyTriangulation(self, silhouette, primaryPlaine):
        # execute delaunays triangulation on silhouette

        # convert silhouette into triangulate parameter
        new_sil = {}
        new_sil['vertices'] = []
        new_sil['segments'] = []

        # create dict of old : new vertex indexes
        new_index = {} # old -> new
        old_index = {} # new -> old

        for index, vert in enumerate(silhouette['vertices']):
            if primaryPlaine == 'x':
                new_sil['vertices'].append([vert.co.y, vert.co.z])
            elif primaryPlaine == 'y':
                new_sil['vertices'].append([vert.co.x, vert.co.z])
            elif primaryPlaine == 'z':
                new_sil['vertices'].append([vert.co.x, vert.co.y])

            new_index[vert.index] = index
            old_index[index] = vert.index

        for segment in silhouette['segments']:
            # append new indexes (through dict)
            new_sil['segments'].append([new_index[segment[0]], new_index[segment[1]]]) 

        print(f"sil vertices: {len(new_sil['vertices'])}")
        print(f"sil segments: {len(new_sil['segments'])}")

        for segment in new_sil['segments']:
            print(f"segment: {segment}")


        # triangulate
        triangulation = triangle.triangulate(new_sil, 'p')

        return triangulation, (old_index, new_index)

    # create bones defined by triangulation
    def defineBones(self, triangulation, silhouette, mesh, index_translate):
        # create bones from trianguleted silhouette
        bones = {}
        bones[0] = []
        bones[1] = []
        bones[2] = []
        bones['branches'] = []

        for key in triangulation.keys():
            print(f"triangulation: {key}")

        print(f"triangulation vertices: {len(triangulation['vertices'])}")
        print(f"{triangulation['vertices']}")
        print("silhouette verts:")

        for vert in silhouette['vertices']:
            print(f"{vert.co.y}, {vert.co.z}")

        for segment in triangulation['segments']:
            print(f"segment: {segment}")


        # go through triangles, categorise them into groups acording to edges (outside or inside edge <- check silhouette edges for outside edges)
        for triangle in triangulation['triangles'].tolist():
            print(f"triangle: {triangle}")

            # create triangle_edges list
            triangle_edges = [(triangle[0], triangle[1]),
                              (triangle[1], triangle[2]),
                              (triangle[2], triangle[0])]


            out_edges = 0
            # check triangle edges
            in_edges = []
            for edge in triangle_edges: # translate segment indexes to old from new
                if edge[0] in index_translate[0] and edge[1] in index_translate[0]:
                    old_edge = (index_translate[0][edge[0]], index_translate[0][edge[1]])
                    # check if trinagle edge is in silhouette segments
                    found = False
                    for sil_edge in silhouette['segments']:
                        if all(index in old_edge for index in sil_edge):
                            out_edges += 1
                            found = True
                            break
                    if not found:
                        in_edges.append(old_edge)


            print(f"triangle: {triangle} outside_edges: {out_edges}")


            # check if index is valid
            valid = True
            for edge in in_edges:
                if edge[0] >= len(mesh.vertices) or edge[1] >= len(mesh.vertices):
                    valid = False


            if out_edges == 2 and len(in_edges) == 1:
                # terminal triangle, center of inner edge ends bone

                if valid:
                    # get edge1 center
                    a = ((mesh.vertices[in_edges[0][0]].co.x + mesh.vertices[in_edges[0][1]].co.x) /2,
                        (mesh.vertices[in_edges[0][0]].co.y + mesh.vertices[in_edges[0][1]].co.y) /2,
                        (mesh.vertices[in_edges[0][0]].co.z + mesh.vertices[in_edges[0][1]].co.z) /2)

                # create new bone
                bones[2].append([a])

            elif out_edges == 1 and len(in_edges) == 2:
                # bone goes through centers of inner edges

                if valid:
                    # get edge1 center
                    a = ((mesh.vertices[in_edges[0][0]].co.x + mesh.vertices[in_edges[0][1]].co.x) /2,
                        (mesh.vertices[in_edges[0][0]].co.y + mesh.vertices[in_edges[0][1]].co.y) /2,
                        (mesh.vertices[in_edges[0][0]].co.z + mesh.vertices[in_edges[0][1]].co.z) /2)
                    # get edge2 center
                    b = ((mesh.vertices[in_edges[1][0]].co.x + mesh.vertices[in_edges[1][1]].co.x) /2,
                        (mesh.vertices[in_edges[1][0]].co.y + mesh.vertices[in_edges[1][1]].co.y) /2,
                        (mesh.vertices[in_edges[1][0]].co.z + mesh.vertices[in_edges[1][1]].co.z) /2)

                # create new bone
                bones[1].append([a,b])


            elif out_edges == 0 and len(in_edges) == 3:
                # bone junction, connects bones from all three edges

                if valid:
                    # get edge1 center
                    a = ((mesh.vertices[in_edges[0][0]].co.x + mesh.vertices[in_edges[0][1]].co.x) /2,
                        (mesh.vertices[in_edges[0][0]].co.y + mesh.vertices[in_edges[0][1]].co.y) /2,
                        (mesh.vertices[in_edges[0][0]].co.z + mesh.vertices[in_edges[0][1]].co.z) /2)
                    # get edge2 center
                    b = ((mesh.vertices[in_edges[1][0]].co.x + mesh.vertices[in_edges[1][1]].co.x) /2,
                        (mesh.vertices[in_edges[1][0]].co.y + mesh.vertices[in_edges[1][1]].co.y) /2,
                        (mesh.vertices[in_edges[1][0]].co.z + mesh.vertices[in_edges[1][1]].co.z) /2)
                    # get edge3 center
                    c = ((mesh.vertices[in_edges[2][0]].co.x + mesh.vertices[in_edges[2][1]].co.x) /2,
                        (mesh.vertices[in_edges[2][0]].co.y + mesh.vertices[in_edges[2][1]].co.y) /2,
                        (mesh.vertices[in_edges[2][0]].co.z + mesh.vertices[in_edges[2][1]].co.z) /2)

                    # create new bone
                    bones[0].append([a,b,c])


        # go through junctions bones[0], if two junctions share edge make shared connection point at midpoint of shared edge
        # first go through junction midpoints as branch starting points and continue connecting edges with same midpoint coordinates
        for junction in bones[0]:
            for edge_midpoint in junction:

                found = False
                for branche in bones['branches']:
                    if branche[1] == edge_midpoint:
                        found = True
                        break

                if not found:

                    new_branch_bones = []
                    last_midpoint = edge_midpoint
                    next_found = True
                    bones_copy = bones[1].copy()
                    while next_found:
                        next_found = False
                        for bone in bones_copy:
                            print(last_midpoint)
                            print(bone)
                            if last_midpoint in bone:
                                print("IN!!")
                                if bone[0] == last_midpoint:
                                    last_midpoint = bone[1]
                                    new_branch_bones.append([bone[0], bone[1]])
                                else:
                                    last_midpoint = bone[0]
                                    new_branch_bones.append([bone[1], bone[0]])
                                bones_copy.remove(bone)
                                next_found = True
                                break

                    if edge_midpoint != last_midpoint:
                        bones['branches'].append([edge_midpoint, last_midpoint, new_branch_bones])
     
        return bones

    # find secondary plaine
    def adjustBones(self, bones):
        # acording to plaine with second largest variance adjust bones in third(primary) axis
        # 1. get secondary plaine
        # 2. get closest 4 vertices and compute centerpoint, set centerpoint as new coordinates 
        return bones


    # transform bones to animation skeleton
    def makeAnimSkeleton(self, bones, mesh):
        
        # make list of centerpoints of junctions, junctions will have index to its centerpoint in list
        # go through junctions and make center connection point
        for index, junction in enumerate(bones[0]):
            # get centerpoint
            centerpoint = (
                (junction[0][0]+junction[1][0]+junction[2][0])/3,
                (junction[0][1]+junction[1][1]+junction[2][1])/3,
                (junction[0][2]+junction[1][2]+junction[2][2])/3)
            
            # add centerpoint to junction
            bones[0][index].append(centerpoint)

        for junction in bones[0]:
            # find bordering junctions
            for border_junction in bones[0]:
                if border_junction != junction:
                    # check if shared midpoint exists
                    for midpoint in junction:
                        if midpoint in border_junction:
                            junction[3] = midpoint
                            border_junction[3] = midpoint

        # find branches connected to junction and change starting or ending vertex to centerpoint 
        for junction in bones[0]:
            for branch in bones['branches']:
                if branch[0] in junction:
                    print(f"start {branch[0]} changed to \n      {junction[3]}") 
                    branch[0] = junction[3]
                    branch[2][0][0] = junction[3]
                    
                elif branch[1] in junction:
                    print(f"end {branch[1]} changed to \n    {junction[3]}")
                    branch[1] = junction[3]
                    branch[2][-1][-1] = junction[3]
                    
        armatureStart = bones[2][0][0]  
        if len(bones[0]) > 0:          
            armatureStart = bones[0][0][3]

        bpy.ops.object.armature_add(
            enter_editmode=True,
            location=armatureStart
        )
        armature = bpy.context.object

        if len(bones[0]) > 0:

            boneNum = 0
            bones[0][0].append(armature.data.edit_bones['Bone'])

            # make copy of branches and after branch complete remove it from branches_copy --> skeletonize only once
            branches = bones['branches'].copy()
            for branch in branches:
                print(f"old point: {branch[0]}")
            for branch in bones['branches']:
                print(f"original point: {branch[0]}")
            # find branches ending in junction
            branches_to_process = []
            for branch in bones['branches']:
                if branch[0] == armatureStart:
                    branches_to_process.append(branch)
                    branches.remove(branch)

            for branch in branches:
                print(f"old point: {branch[0]}")
            for branch in bones['branches']:
                print(f"original point: {branch[0]}")

            print("\n=========== STARTING BRANCHES: =================\n")
            print(f"starting point: {armatureStart}")
            print(len(bones['branches']))
            print(len(branches))
            print(len(branches_to_process))


            # connect junctions first and set bone references to junctions
            while(len(branches_to_process) > 0):
                branch = branches_to_process[0]
                for junction in bones[0]:
                    if branch[0] in junction:
                        # set lastBoneReference to junction bone
                        lastBoneReference = junction[4]
                        lastBone = None
                        skelBoneStart = branch[2][0][0]
                        for bone in branch[2]:
                    
                            #check angle 
                            if lastBone != None:
                                # get angle between bone and lastBone
                                vec1 = (lastBone[0][0] - lastBone[1][0], lastBone[0][1] - lastBone[1][1], lastBone[0][2] - lastBone[1][2])

                                vec2 = (bone[0][0] - bone[1][0], bone[0][1] - bone[1][1], bone[0][2] - bone[1][2])


                                angle = math.atan2(vec1[1], vec1[0]) - math.atan2(vec2[1], vec2[0])
                                angle = abs(angle)
                                    

                                # if angle > bone_angle make new skel_bone
                                if angle > bpy.context.scene.method_3D_silhouette.bone_angle:
                                    new_bone = armature.data.edit_bones.new('bone'+str(boneNum))
                                    new_bone.head = (skelBoneStart[0] - armatureStart[0], skelBoneStart[1] - armatureStart[1], skelBoneStart[2] - armatureStart[2])
                                    new_bone.tail = (bone[1][0] - armatureStart[0], bone[1][1] - armatureStart[1], bone[1][2] - armatureStart[2])
                                    new_bone.parent = lastBoneReference
                                    lastBoneReference = new_bone
                                    skelBoneStart = bone[1]
                                    boneNum += 1

                            lastBone = bone

                        # check if lastBone is endpoint of branch, if not create final skeleton bone
                        if skelBoneStart != branch[1]:
                            new_bone = armature.data.edit_bones.new('bone'+str(boneNum))
                            new_bone.head = (skelBoneStart[0] - armatureStart[0], skelBoneStart[1] - armatureStart[1], skelBoneStart[2] - armatureStart[2])
                            new_bone.tail = (branch[1][0] - armatureStart[0], branch[1][1] - armatureStart[1], branch[1][2] - armatureStart[2])
                            new_bone.parent = lastBoneReference
                            lastBoneReference = new_bone
                            boneNum += 1


                        # add end junction bone reference
                        for end_junction in bones[0]:
                            if branch[1] == end_junction[3]:
                                end_junction.append(lastBoneReference)
                                for new_branch in bones['branches']:
                                    if new_branch[0] == end_junction[3]:
                                        if new_branch in branches:
                                            branches_to_process.append(new_branch)
                                            branches.remove(new_branch)

                        branches_to_process.remove(branch)
                        break



        return armature

    


    def rig(self, obj):
        ''' implement method to rig obj (obj -- active object) '''

        time1 = time.time()


        bpy.ops.object.mode_set(mode='OBJECT')

        # implement method HERE
        mesh = obj.data

        primaryPlaine = self.getPrimaryPlaine(mesh)

        silhouette = self.get3Dsilhouette(mesh, primaryPlaine)
        
        # test silhouette result -- select vertices
        for vert in silhouette['vertices']:
            vert.select = True
            print(f"vertex index: {vert.index}")
        
        for edge in silhouette['segments']:
            for vert_index in edge:
                mesh.vertices[vert_index].select = True
            print(f"edge: {edge}")


        triangulation, index_translate = self.deulaneyTriangulation(silhouette, primaryPlaine)
        
        '''# show triangulation
        for edge in triangulation['segments']:
            # create edge
            mesh.edges.add(1)
            new_edge = mesh.edges[-1]
            new_edge.vertices = (index_translate[0][edge[0].item()], index_translate[0][edge[1].item()]) # translate vert index back to old one
        '''
        bones = self.defineBones(triangulation, silhouette, mesh, index_translate)

        armature = self.makeAnimSkeleton(bones, mesh)


        rigger_method.rigged_object = obj
        rigger_method.armature = armature

        time2 = time.time()

        print(f"===============================\nrig time: {time2 - time1}")


    def additional_ui(self, layout, context):
        ''' add additional method UI elements'''
        box = layout.box()
        box.operator('view3d.skinning_operator', text="Skinning")
        box.prop(context.scene.method_3D_silhouette_A, "r_multiplier")
        box.prop(context.scene.method_3D_silhouette_A, "r_ignore")
        box.prop(context.scene.method_3D_silhouette_A, "bone_angle")


# create prop enum for parameters like r_multilpier, r_ignore, bone_angle
class method_3D_silhouette_A_Props(bpy.types.PropertyGroup):

    r_multiplier: bpy.props.FloatProperty(name="r mul", description="Multiplier of neighbour area size", default=1., min=0.)
    r_ignore: bpy.props.FloatProperty(name="r ignore", description="Multiplier of area where neighbor vertices are too close and will be ignored", default=.4, min=0.)
    bone_angle: bpy.props.FloatProperty(name="bone angle", description="Minimum angle between bones, used for reduction of bones", default=math.pi/10, soft_min=0., soft_max=math.pi*2)

bpy.utils.register_class(method_3D_silhouette_A_Props)
bpy.types.Scene.method_3D_silhouette_A = bpy.props.PointerProperty(type=method_3D_silhouette_A_Props)