# Rigger add-on for Blender
# Copyright (C) 2020  Rostislav Horák
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



bl_info = {
    "name" : "Rigger",
    "author" : "Rostislav Horak",
    "description" : "Automatic rigging tool",
    "blender" : (2, 91, 0),
    "version" : (1, 0, 0),
    "location" : "View3D",
    "warning" : "",
    "category" : "Rigging"
}

import bpy



from . rigger_method import rigger_method
from . rig_op import Rigger_Operator
from . skin_op import Skinning_Operator
from . rigger_panel import Rigger_Panel, Rigger_Props

classes = (Rigger_Operator, Skinning_Operator, Rigger_Panel, Rigger_Props)

def register():
    # import rigging methods
    rigger_method.import_methods()

    for klass in classes:
        bpy.utils.register_class(klass)

    bpy.types.Scene.rigger = bpy.props.PointerProperty(type=Rigger_Props)




def unregister():
    for klass in classes:
        bpy.utils.unregister_class(klass)

    rigger_method.panel_list = []

    del bpy.types.Scene.rigger